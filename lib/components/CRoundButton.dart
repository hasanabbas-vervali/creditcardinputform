import 'package:flutter/material.dart';

Widget CButton({
  BuildContext context,
  String Name,
  Function onPress,
}) {
  return Padding(
    padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.060,
        left: MediaQuery.of(context).size.width * 0.040,
        right: MediaQuery.of(context).size.width * 0.040),
    child: RaisedButton(
      padding: EdgeInsets.all(15),
      color: Theme.of(context).primaryColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      onPressed: onPress,
      child: Text(
        '$Name'.toUpperCase(),
        style: TextStyle(
            color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
      ),
    ),
  );
}
