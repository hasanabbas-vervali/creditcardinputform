import 'package:credit_card_input_form/components/yellow_border.dart';
import 'package:credit_card_input_form/constants/captions.dart';
import 'package:credit_card_input_form/constants/constanst.dart';
import 'package:credit_card_input_form/util/util.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'card_logo.dart';
import 'card_name.dart';
import 'card_number.dart';
import 'card_valid.dart';

class FrontCardView extends StatelessWidget {
  final height;
  final decoration;

  FrontCardView({this.height, this.decoration});

  @override
  Widget build(BuildContext context) {
    final captions = Provider.of<Captions>(context);

    return Container(
      margin: EdgeInsets.only(bottom: 5),
      height: height,
      decoration: decoration,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Stack(
          children: <Widget>[
            YellowBorder(),
            Align(
              alignment: Alignment.centerLeft,
              child: CardNumber(),
            ),
            Positioned(
              top: -15,
              bottom: 0,
              left: 0,
              right: 0,
              child: Align(
                alignment: Alignment.topLeft,
                child: CardLogo(),
              ),
            ),
            Align(
                alignment: Alignment.topRight,
                child: BankName(
                  bankName: captions.getCaption('BANKNAME'),
                )),
            SizedBox(
              height: 50,
            ),
            Positioned(
              top: 20,
              child: Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 10, left: 5, bottom: 10, right: 0),
                  child: sim,
                ),
              ),
            ),
            Positioned(
              top: 20,
              left: 0,
              right: 1,
              child: Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: wifi,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      captions.getCaption('CARDHOLDER_NAME').toUpperCase(),
                      style: kTextStyle,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CardName(),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      captions.getCaption('VALID_THRU').toUpperCase(),
                      style: kTextStyle,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CardValid(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
